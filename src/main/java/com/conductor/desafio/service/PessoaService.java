package com.conductor.desafio.service;

import org.springframework.stereotype.Service;

import com.conductor.desafio.model.Pessoa;
/**
 * @author janaina militão
 */

@Service
public class PessoaService extends GenericService<Pessoa>{

}
