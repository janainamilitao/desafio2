package com.conductor.desafio.service;

import org.springframework.stereotype.Service;

import com.conductor.desafio.model.Conta;

/**
 * @author janaina militão
 */

@Service
public class ContaService extends GenericService<Conta>{

}
