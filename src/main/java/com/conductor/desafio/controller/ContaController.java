package com.conductor.desafio.controller;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.conductor.desafio.exception.ContaNotFoundException;
import com.conductor.desafio.exception.PessoaNotFoundException;
import com.conductor.desafio.model.Conta;
import com.conductor.desafio.model.Pessoa;
import com.conductor.desafio.service.ContaService;
import com.conductor.desafio.service.PessoaService;
/***
 * @author janaina
 */

@RestController
public class ContaController {
	
	@Autowired
	private ContaService contaService;
	
	@Autowired
	private PessoaService pessoaService;
	
	/****
	 * Cria uma conta recebendo como paramentro ID de pessoa já criada e caso não exista lança uma exceção
	 * @param conta
	 * @param idPessoa
	 * @return Conta
	 * @throws Exception
	 */
	@PostMapping("/criarConta/{idPessoa}")
	public Conta criarConta(@RequestBody Conta conta, @PathVariable("idPessoa") Long idPessoa) throws Exception {
		
		Optional<Pessoa> pessoa = pessoaService.findById(idPessoa);
			
		if(pessoa.isPresent()) {
			conta.setPessoa(pessoa.get());
			conta.setDataCriacao(new Date());
			return contaService.save(conta);
		}else {			
			throw new PessoaNotFoundException();
		}		
	}
	/*****
	 * Consulta o saldo recebendo como paramentro ID de uma conta e caso não exista lança uma exceção.
	 * @param idConta
	 * @return String
	 * @throws Exception
	 */
	@GetMapping("/consultaSaldo/{idConta}")
	public String consultarSaldo(@PathVariable("idConta") Long idConta) throws Exception {	
		
		Optional<Conta> conta = contaService.findById(idConta);		
		
		if(conta.isPresent()) {	
			return "Saldo atual: "+conta.get().getSaldo();
		}
		else {
			throw new ContaNotFoundException();
		}
	}
	
	/**
	 * Bloqueio de conta recebendo como paramentro ID  da conta e caso não exista lança exceção 
	 * @param idConta
	 * @return Conta
	 * @throws Exception
	 */
	
	@PostMapping("/bloquearConta/{idConta}")
	public Conta bloquearConta(@PathVariable("idConta") Long idConta) throws Exception {
		
		Optional<Conta> contaOptional = contaService.findById(idConta);		
		
		if(contaOptional.isPresent()) {	
			Conta conta = contaOptional.get();
			conta.setFlagAtivo(false);
			contaService.save(conta);
			return conta;
		}else {
			throw new ContaNotFoundException();
		}		
	}
	/***
	 *  Desbloqueia conta recebendo como paramentro ID da conta e caso não exista lança exceção.
	 * @param idConta
	 * @return Conta
	 * @throws Exception
	 */
	@PostMapping("/desbloquearConta/{idConta}")
	public Conta desbloquearConta(@PathVariable("idConta") Long idConta) throws Exception {
		
		Optional<Conta> contaOptional = contaService.findById(idConta);		
		
		if(contaOptional.isPresent()) {	
			Conta conta = contaOptional.get();
			conta.setFlagAtivo(true);
			contaService.save(conta);
			return conta;
		}else {
			throw new ContaNotFoundException();
		}		
	}
	
}
