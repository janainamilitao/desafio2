package com.conductor.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conductor.desafio.model.Conta;
import com.conductor.desafio.model.Transacao;

import java.util.Date;
import java.util.List;

/**
 * @author janaina militão
 */

@Repository
public interface TransacaoRepository extends JpaRepository<Transacao, Long>{	
	
	public List<Transacao> findByConta(Conta conta);
	
	public List<Transacao> findByContaAndDataTransacaoBetween(Conta conta, Date inicio, Date fim);
	
}
