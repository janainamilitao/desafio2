package com.conductor.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conductor.desafio.model.Conta;

/**
 * @author janaina militão
 */

@Repository
public interface ContaRepository extends JpaRepository<Conta, Long> {

}
