package com.conductor.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.conductor.desafio.model.Pessoa;

/**
 * @author janaina militão
 */

@Repository
public interface PessoaRepository  extends JpaRepository<Pessoa, Long>{

}
