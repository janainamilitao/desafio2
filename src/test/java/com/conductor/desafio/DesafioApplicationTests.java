package com.conductor.desafio;


import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.conductor.desafio.model.Conta;
import com.conductor.desafio.model.Pessoa;
import com.conductor.desafio.model.TipoContaEnum;
import com.conductor.desafio.model.TipoTransacaoEnum;
import com.conductor.desafio.model.Transacao;
/**
 * @author janaina militão
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class DesafioApplicationTests {
	
	Conta conta;
	Pessoa pessoa;
	Transacao transacao;
	
	@Before
	public void setUp(){
		this.pessoa = criarPessoa();
		this.conta = criarConta();
	}

	private Pessoa criarPessoa() {
		Pessoa pessoa = new Pessoa();
		pessoa.setCpf("Luiz Carlos Mendonça");
		pessoa.setNome("97794126037");
		pessoa.setDataNascimento(new Date());
		return null;
	}

	private Conta criarConta() {
		Conta conta = new Conta();
		conta.setId(1L);
		conta.setDataCriacao(new Date());
		conta.setFlagAtivo(true);
		conta.setLimiteSaqueDiario(new BigDecimal(500.00));
		conta.setSaldo(new BigDecimal(3000.00));
		conta.setTipoConta(TipoContaEnum.CONTA_CORRENTE);
		conta.setPessoa(this.pessoa);
		return conta;
	}

	@Test
	public void deposito() {
		Transacao transacao = new Transacao();
		transacao.setDataTransacao(new Date());		
		transacao.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacao.setValor(new BigDecimal(300.00));
		transacao.setConta(this.conta);
		
		BigDecimal saldoAnterior = conta.getSaldo();		
		conta.setSaldo(conta.getSaldo().add(transacao.getValor()));			
		boolean depositoCorreto =  conta.getSaldo().longValue()>saldoAnterior.longValue();
		
		assertTrue(depositoCorreto);
		
	}
	
	@Test
	public void saque() {
		Transacao transacao = new Transacao();
		transacao.setDataTransacao(new Date());		
		transacao.setTipoTransacao(TipoTransacaoEnum.SAQUE);
		transacao.setValor(new BigDecimal(200.00));
		transacao.setConta(this.conta);
		
		BigDecimal saldoAnterior = conta.getSaldo();		
		conta.setSaldo(conta.getSaldo().subtract(transacao.getValor()));			
		boolean saqueCorreto =  conta.getSaldo().longValue()<saldoAnterior.longValue();
		
		assertTrue(saqueCorreto);		
	}

}
