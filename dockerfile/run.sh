#!/bin/bash
# Iniciando o microsservico no Ubuntu

#------------------
# VARIAVEIS
#------------------
DB_URL=$1
DB_USER=$2
DB_PASS=$3
MEMORY_JVM=$4

SERVICE_DIR_BASE=/home/desafio/
SERVICE_DIR=$SERVICE_DIR_BASE/app
LOGFILE=$SERVICE_DIR_BASE/log/app.log
SERVICE_APP_FILE=desafio.jar
SERVICE_APP_SQL_FILE=desafio_init.sql
ADMIN_USER=desafio



#------------------
# MAIN
#------------------

chown -R $ADMIN_USER:$ADMIN_USER $SERVICE_DIR_BASE/
chmod -R 755 $SERVICE_DIR_BASE/

cd $SERVICE_DIR/


/bin/su -c "/usr/bin/java \
            -Xmx${MEMORY_JVM}m \
            -Dspring.datasource.url='jdbc:$DB_URL' \
            -Dspring.datasource.username=$DB_USER \
            -Dspring.datasource.password=$DB_PASS \
            -jar $SERVICE_DIR/$SERVICE_APP_FILE --logging.file=$LOGFILE" $ADMIN_USER


cd -
