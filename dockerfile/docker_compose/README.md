# Instruções para baixar e compilar a imagem Docker

## Configurações prévias

Realize as configurações prévias citadas [aqui](../README_DOCKER_APP.md).

# Instruções para baixar e iniciar o Docker Compose

Para instalar o Docker Compose na versão 1.22.0, por exemplo, use os
comandos a seguir.

```sh
sudo su
COMPOSE_VERSION=1.22.0
curl -L https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` > /usr/bin/docker-compose
chmod 777 /usr/bin/docker-compose
exit
```

## Inicie os conteiners

Use os comandos a seguir para iniciar os conteineres.

```sh
cd docker_compose/
docker-compose -f docker-compose_desafio-services.yml up --build
```

Para visualizar os conteineres em execução, digite o comando abaixo.

```sh
cd docker_compose/
docker-compose -f docker-compose_desafio-services.yml ps
```

Para visualizar os logs de todos os conteineres, use o comando abaixo.

```sh
cd docker_compose/
docker-compose -f docker-compose_desafio-services.yml logs
```

Para visualizar os logs de um conteiner especifico, digite:

```sh
cd docker_compose/
docker-compose -f docker-compose_desafio-services.yml logs NOME_SERVICO
```

O nome do serviço é declarado no arquivo ``docker-compose_prova-services.yml``. Exemplo

```sh
cd docker_compose/
docker-compose -f docker-compose_desafio-services.yml logs app
```

## Acesso a App

Acesse a interface da APP através da URL http://localhost:8080.

## Pare os conteiners

Use os comandos a seguir para parar e remover os conteineres.

```sh
cd docker_compose/
docker-compose -f docker-compose_desafio-services.yml down
```

Para obter mais informações sobre o Docker Compose acesse:

* [https://docs.docker.com/compose/reference/](https://docs.docker.com/compose/reference/)
* [https://docs.docker.com/compose/compose-file/](https://docs.docker.com/compose/compose-file/)
