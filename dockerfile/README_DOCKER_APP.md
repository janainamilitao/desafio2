# Instruções para baixar e compilar a imagem Docker

## Configurações prévias

* Instale o Docker-CE seguindo as instruções das páginas abaixo de acordo com a
distribuição GNU/Linux.

No Ubuntu:
https://docs.docker.com/install/linux/docker-ce/ubuntu/

No CentOS:
https://docs.docker.com/install/linux/docker-ce/centos/

No Debian:
https://docs.docker.com/install/linux/docker-ce/debian/

Altere o valor da variárel ``USUARIO`` pelo nome da sua conta no S.O no qual
o Docker foi instalado e execute os comandos abaixo para adicionar o seu usuário
 ao grupo Docker.

```bash
USUARIO="seu-nome-usuario"
sudo usermod -aG docker $USUARIO
sudo setfacl -m user:$USUARIO:rw /var/run/docker.sock
```

* Siga as instruções [daqui](docker_compose/README.md) para subir todos os
conteineres com o Docker Compose.

## Compilando a imagem Docker da aplicação

Considerando que você já realizou as configurações prévias, use o
comando a seguir para gerar uma imagem do microsserviço.

* Baixe o código do repositório Git com o comando a seguir.

```sh
git clone https://jmilitao@bitbucket.org/jmilitao/desafio2.git
cd desafio2/dockerfile
docker build -t desafio/app .
```

* Envie a imagem para Docker Hub com os comandos abaixo.

```sh
docker login -u USERNAME -p PASS

docker tag desafio/app jmilitao/desafio
docker push jmilitao/desafio
```

## Iniciando o conteiner do Banco de dados

* Execute o comando do PostgreSQL com os comandos a seguir.

```sh
mkdir -p /docker/postgresql/data
chown -R 999:999 /docker/postgresql/data

docker run -d -p 5432:5432 \
--name postgresql \
--restart=always \
-v /docker/postgresql/data:/var/lib/postgresql/data \
-e POSTGRES_PASSWORD=postgres \
-e POSTGRES_USER=postgres \
-e POSTGRES_DB=desafio2 \
postgres
```

Configuração dos bancos de dados a ser criado.

| Serviço    | Banco    | Usuário   | Senha    |
|------------|----------|-----------|----------|
|App Desafio | desafio2 | postgres  | postgres |

## Iniciando o conteiner da aplicação

Use o comando a seguir para iniciar um conteiner da aplicação.

```sh
docker run -d -p 8080:8080 \
--name desafio-app \
-e DATASOURCE_URL="postgresql://172.17.0.1:5432/desafio2" \
-e DATASOURCE_USERNAME="postgres" \
-e DATASOURCE_PASSWORD="postgres" \
-e MEMORY_JVM="512" \
jmilitao/desafio
```

## Variáveis que podem ser customizadas ao iniciar o conteiner.

Nome: ``DATASOURCE_URL``<br>
Valor padrão: ``postgresql://172.17.0.1:5432/desafio2``<br>
Descrição: Define o tipo do SGBD, host, porta e nome do database a ser usado pela aplicação.

Nome: ``DATASOURCE_USERNAME``<br>
Valor padrão: ``postgres``<br>
Descrição: Define o nome do usuario que acessará o database.

Nome: ``DATASOURCE_PASSWORD``<br>
Valor padrão: ``postgres``<br>
Descrição: Define a senha do usuario que acessará o database.

Nome: ``MEMORY_JVM``<br>
Valor padrão: ``512``<br>
Descrição: Define o limite mínimo em MB da memória da JVM para esse microsserviço.

## Visualizando o log da aplicação

```sh
docker logs -f desafio-app
```

## Removendo o conteiner da aplicação

```sh
docker rm -f desafio-app
```

## Referências:

https://docs.docker.com/engine/tutorials/dockerimages/

https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04

https://docs.docker.com/engine/reference/commandline/commit/

https://www.digitalocean.com/community/tutorials/docker-explained-how-to-create-docker-containers-running-memcached
