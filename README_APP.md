## Executando a aplicação usando o Docker

* Instale o Docker-CE seguindo as instruções das páginas abaixo de acordo com a
distribuição GNU/Linux.

No Ubuntu:
https://docs.docker.com/install/linux/docker-ce/ubuntu

No CentOS:
https://docs.docker.com/install/linux/docker-ce/centos

No Debian:
https://docs.docker.com/install/linux/docker-ce/debian

* Altere o valor da variárel ``USUARIO`` pelo nome da sua conta no S.O no qual
o Docker foi instalado e execute os comandos abaixo para adicionar o seu usuário
 ao grupo Docker.

```bash
USUARIO="seu-nome-usuario"
sudo usermod -aG docker $USUARIO
sudo setfacl -m user:$USUARIO:rw /var/run/docker.sock
```

* Siga as instruções da página https://hub.docker.com/r/jmilitao/desafio para
subir o conteiner do banco de dados e da aplicação.

* Os arquivos que deram origem a imagem Docker da aplicação estão no
diretório ``dockerfile`` do projeto ``https://bitbucket.org/janainamilitao/desafio2``.

* Popule o banco de dados usando o script ``desafio_init.sql``, que encontra-se na raiz do projeto (https://bitbucket.org/janainamilitao/desafio2), executando o seguinte comando.

```bash
chmod +rx desafio_init.sql
PGPASSWORD=postgres /usr/bin/psql postgresql://IP_POSTGRESQL_SERVER:5432/desafio2 -U postgres -f desafio_init.sql
```

## Testando a aplicação via API Rest

1) Instale o Postman (https://www.getpostman.com) ou use uma ferramenta similar
para testar o uso da API Rest.

2) A seguir será exibido um exemplo de como acessar cada path junto com imagem
mostrando dados de teste e retorno usando o Postman.

3) Path que realiza a criação de uma conta.

```
MÉTODO: POST
URL:  http://localhost:8080/criarConta/{idPessoa}
JSON:
{
 "saldo":"4500.00",
 "limiteSaqueDiario":"1500.00",
 "flagAtivo": true,
 "tipoConta": 2
}
```

Possíveis parâmetros para ``tipoConta``:

* 0 -> Conta Corrente
* 1 -> Conta Poupança
* 2 -> Conta Salário

Captura de tela no Postman - Criar uma conta

![alt text](images/01_criar_conta.png "Criando uma conta")


4) Path que realiza operação de depósito em uma conta.

```
MÉTODO: POST
URL: http://localhost:8080/deposito/{idConta}
JSON:
{
 "valor":"255.00"
}
```

Captura de tela no Postman - Depósito na conta ativa

![alt text](images/02_deposito_conta_ativa.png "Depositando na conta ativa")

Captura de tela no Postman - Depósito na conta inativa

![alt text](images/03_deposito_conta_inativa.png "Depositando na conta inativa")

5) Path que realiza operação de saque em uma conta.

```
MÉTODO: POST
URL: http://localhost:8080/saque/{idConta}
JSON:
{
 "valor":"50.00"
}
```

Captura de tela no Postman - Saque de uma conta ativa

![alt text](images/04_saque_conta_ativa.png "Sacando de uma conta ativa")

Captura de tela no Postman - Saque de uma conta inativa

![alt text](images/05_saque_conta_inativa.png "Sacando de uma conta inativa")

6) Path que realiza operação de consulta de saldo em determinada conta.

```
MÉTODO: GET
URL: http://localhost:8080/consultaSaldo/{idConta}
```

Captura de tela no Postman - Consulta saldo de uma conta.

![alt text](images/06_consultar_saldo.png "Consultando o saldo de uma conta")

7) Path que realiza o bloqueio de uma conta.

```
MÉTODO: POST
URL: http://localhost:8080/bloquearConta/{idConta}
```

Captura de tela no Postman - Bloqueio de uma conta

![alt text](images/07_bloquear_conta.png "Bloqueando uma conta")


8) Path que realiza o desbloqueio de uma conta.

```
MÉTODO: POST
URL: http://localhost:8080/desbloquearConta/{idConta}
```

Captura de tela no Postman - Desbloqueio de uma conta

![alt text](images/08_desbloquear_conta.png "Desbloqueando uma conta")


9) Path que exibe o extrato de transações de uma conta.

```
MÉTODO: GET
URL: http://localhost:8080/extrato/{idConta}
```

Captura de tela no Postman - Historico de transações de uma conta

![alt text](images/09_extrato.png "Exibindo as transações de uma conta")


10) Path que exibe extrato da conta por período.

```
MÉTODO: GET
URL: http://localhost:8080/extratoPeriodo/{idConta}/{mes}/{ano}
```

Captura de tela no Postman - Extrato por período de uma conta

![alt text](images/10_extrato_periodo.png "Exibindo o extrato por período")
